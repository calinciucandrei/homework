package Course3;

import android.renderscript.Sampler;
import android.util.Size;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class course_3 {

    static void Convert() {
        String[] my_array = new String[]{"Python", "JAVA", "PHP", "Perl", "C#", "C++"};
        ArrayList<String> list = new ArrayList<String>(Arrays.asList(my_array));

        System.out.println(list);

    }

    static void switchPairs() {
        String[] my_array = new String[]{"four", "score", "and", "seven", "years", "ago"};
        ArrayList<String> list = new ArrayList<String>(Arrays.asList(my_array));
        System.out.println(list);

        if (list.size() % 2 == 0) {
            for (int i = 0; i <= list.size() - 1; i += 2)

                Collections.swap(list, i, (i + 1));
        } else {
            for (int i = 0; i < list.size() - 1; i += 2)
                Collections.swap(list, i, (i + 1));
        }
        System.out.println(list);


    }

    static void markLength4() {
        String[] my_array = new String[]{"this", "is", "lots", "of", "fun", "for", "every", "Java", "programmer"};
        ArrayList<String> list = new ArrayList<String>(Arrays.asList(my_array));
        System.out.println(list);

        int length = list.size();
        for (int i = 0; i <= length - 1; i++) {
            if (list.get(i).length() == 4) {
                list.add(i, "****");
                length++;
                i++;
            }

        }


        System.out.println(list);


    }

    public static void main(String[] args) {

        Convert();
        switchPairs();
        markLength4();


    }


}

