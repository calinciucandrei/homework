package Course4.Problem1;

public class NumberOfCaracters {

    public static void NumberOfCaracters() {

        int ci, i, j, k, l = 0;
        String str, str1;
        char c, ch;
        str = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book";


        i = str.length();
        for (c = 'A'; c <= 'z'; c++) {
            k = 0;
            for (j = 0; j < i; j++) {
                ch = str.charAt(j);
                if (ch == c) {
                    k++;
                }
            }
            if (k > 0) {
                System.out.println("The character " + c + " has occurred for " + k + " times");
            }
        }
    }

    public static void main(String[] args) {
        NumberOfCaracters();
    }
}
