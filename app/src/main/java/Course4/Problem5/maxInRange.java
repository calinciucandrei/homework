package Course4.Problem5;
//https://books.trinket.io/thinkjava/chapter8.html
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class maxInRange {

    public static int maxInRange(int[] a, int lowIndex, int highIndex) {
        int maxim = 0;

        if (lowIndex == highIndex)
            return a[lowIndex];
        else {
            for (int i = lowIndex; i < highIndex; i++) {
                if (maxim < a[i]) {
                    maxim = a[i];
                }
            }
        }
        return maxim;
    }


    public static void main(String args[]) {
        int[] my_array = new int[]{1, 3, 5, 7, 2, 56, 8, 4, 12, 7, 5, 2, 7, 9, 2, 19, 2, 3, 54};
        int lowIndex, highIndex;
        Scanner keyboard = new Scanner(System.in);
        System.out.print("LowIndex=");
        lowIndex = keyboard.nextInt();

        do {
            System.out.print("HighIndex=");
            highIndex = keyboard.nextInt();

        } while (highIndex > my_array.length);


        System.out.println(maxInRange(my_array, lowIndex, highIndex));

    }

}
