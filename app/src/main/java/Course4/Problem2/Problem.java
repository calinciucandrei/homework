package Course4.Problem2;

public class Problem {

    private int numberofStudents;
    private int numberofProjects;


    public Problem(int a, int b) {
        this.numberofStudents = a;
        this.numberofProjects = b;
    }

    public int getNumberofStudents() {
        return numberofStudents;
    }

    public void setNumberofStudents(int numberofStudents) {
        this.numberofStudents = numberofStudents;
    }

    public int getNumberofProjects() {
        return numberofProjects;
    }

    public void setNumberofProjects(int numberofProjects) {
        this.numberofProjects = numberofProjects;
    }
}
