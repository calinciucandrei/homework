package Course4.Problem3;

import java.util.Scanner;
import java.util.Random;

public class randomInt {

    public static int randomInt(int low, int high){

        Random r = new Random();
        int Result = r.nextInt(high-low) + low;
        return Result;
    }

    public static void main(String args [])
    {

        Scanner keyboard= new Scanner(System.in);
        int low=keyboard.nextInt();
        int high=keyboard.nextInt();


        System.out.println(randomInt(low,high));

    }
}
