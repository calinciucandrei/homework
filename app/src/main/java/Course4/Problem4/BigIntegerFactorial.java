package Course4.Problem4;
import java.util.Scanner;

public class BigIntegerFactorial {

    public static void main(String[] args) {
        Scanner findit = new Scanner(System.in);
        System.out.println("Enter a number to find the factorial: ");
        int x = findit.nextInt();
        int answer = factorial(x);
        System.out.println(answer);
    }

    public static int factorial(int x) {
        int answer = 1;
        for (int i = 1; i <= x; i++) {
            answer = answer * i;
        }
        return answer;
    }
}